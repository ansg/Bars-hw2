﻿

namespace ConsoleApp1
{
    public interface IShape
    {
        double GetPerimeter();
        double GetArea();
    }
}