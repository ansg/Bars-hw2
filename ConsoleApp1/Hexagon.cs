﻿namespace ConsoleApp1
{
    public class Hexagon : IShape
    {
        private double _side;

        public Hexagon(double side)
        {
            _side = side;
        }

        public double GetPerimeter()
        {
            return 6 * _side;
        }

        public double GetArea()
        {
            return (3 * 1.732050807568877 * _side * _side) / 2;
        }
    }
}