﻿namespace ConsoleApp1
{
    public class Square : IShape
    {
        private double _side;

        public Square(double side)
        {
            _side = side;
        }

        public double GetPerimeter()
        {
            return 4 * _side;
        }

        public double GetArea()
        {
            return _side * _side;
        }
    }
}