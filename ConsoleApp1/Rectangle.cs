﻿namespace ConsoleApp1
{
    public class Rectangle : IShape
    {

        private double _sideA;
        private double _sideB;

        public Rectangle(double A, double B)
        {
            _sideA = A;
            _sideB = B;
        }

        public double GetPerimeter()
        {
            return 2 * _sideA + 2 * _sideB;
        }

        public double GetArea()
        {
            return _sideA * _sideB;
        }
    }
}