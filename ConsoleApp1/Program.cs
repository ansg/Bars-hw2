﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string c;
            string input;
            double side;
            double radius;
            Console.WriteLine("Chose figure: \n 1 - Square \n 2 - Rectangle \n 3 - Hexagon \n 4 - Circle \n 5 - Triangle \n 6 - Ellipse");
            c = Console.ReadLine() ;
            //Console.Write(c);
            switch (c)
            {
                case "1":
                    Console.WriteLine("Input square side length:");
                    input = Console.ReadLine();
                    side = Convert.ToDouble(input);
                    var S = new Square(side);
                    Console.WriteLine("Perimeter: " + S.GetPerimeter() + " Area: " + S.GetArea());
                    break;
                case "2":
                    Console.WriteLine("Input rectangle side length:");
                    input = Console.ReadLine();
                    side = Convert.ToDouble(input);
                    var R = new Square(side);
                    Console.WriteLine("Perimeter: " + R.GetPerimeter() + " Area: " + R.GetArea());
                    break;
                case "3":
                    Console.WriteLine("Input hexagon side length:");
                    input = Console.ReadLine();
                    side = Convert.ToDouble(input);
                    var H = new Hexagon(side);
                    Console.WriteLine("Perimeter: " + H.GetPerimeter() + " Area: " + H.GetArea());
                    break;
                case "4":
                    Console.WriteLine("Input circle radius length:");
                    input = Console.ReadLine();
                    radius = Convert.ToDouble(input);
                    var C = new Circle(radius);
                    Console.WriteLine("Perimeter: " + C.GetPerimeter() + " Area: " + C.GetArea());
                    break;
                case "5":
                    Console.WriteLine("Input triangle first side length:");
                    input = Console.ReadLine();
                    double sideA = Convert.ToDouble(input);
                    Console.WriteLine("Input triangle second side length:");
                    input = Console.ReadLine();
                    double sideB = Convert.ToDouble(input);
                    Console.WriteLine("Input triangle third side length:");
                    input = Console.ReadLine();
                    double sideC = Convert.ToDouble(input);
                    var T = new Triangle(sideA, sideB, sideC);
                    Console.WriteLine("Perimeter: " + T.GetPerimeter() + " Area: " + T.GetArea());
                    break;
                case "6":
                    Console.WriteLine("Input ellipse big axis length:");
                    input = Console.ReadLine();
                    double bigA = Convert.ToDouble(input);
                    Console.WriteLine("Input ellipse small axis length:");
                    input = Console.ReadLine();
                    double smallA = Convert.ToDouble(input);
                    var E = new Ellipse(bigA, smallA);
                    Console.WriteLine("Perimeter: " + E.GetPerimeter() + " Area: " + E.GetArea());
                    break;
            }
        }
    }
}
