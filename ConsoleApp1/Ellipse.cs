﻿namespace ConsoleApp1
{
    public class Ellipse : IShape
    {
        private double _bigAxis;
        private double _smallAxis;

        public Ellipse(double bigAxis, double smallAxis)
        {
            _bigAxis = bigAxis;
            _smallAxis = smallAxis;
        }

        public double GetPerimeter()
        {
            return 4 * (3.1415926535897932384626433832795 * _bigAxis * _smallAxis + (_bigAxis - _smallAxis)) /
                   (_bigAxis + _smallAxis);
        }

        public double GetArea()
        {
            return 3.1415926535897932384626433832795 * _bigAxis * _smallAxis;
        }
    }
}