﻿namespace ConsoleApp1
{
    public class Circle : IShape
    {
        private double _radius;

        public Circle(double radius)
        {
            _radius = radius;
        }

        public double GetPerimeter()
        {
            return 2 * 3.1415926535897932384626433832795 * _radius;
        }

        public double GetArea()
        {
            return 3.1415926535897932384626433832795 * _radius * _radius;
        }
    }
}